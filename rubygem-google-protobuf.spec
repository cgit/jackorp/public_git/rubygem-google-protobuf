# Generated from google-protobuf-3.22.2.gem by gem2rpm -*- rpm-spec -*-
%global gem_name google-protobuf

Name: rubygem-%{gem_name}
Version: 3.22.2
Release: 1%{?dist}
Summary: Protocol Buffers
License: BSD-3-Clause
URL: https://developers.google.com/protocol-buffers
Source0: %{gem_name}-%{version}.gem

# Wrapping does not work with LTO.
# https://gcc.gnu.org/bugzilla/show_bug.cgi?id=88643
# Additionaly the wrapping exists for extending compatibility with
# multiple glibc versions. We do not care about that.
# https://github.com/protocolbuffers/protobuf/issues/11935
Patch0: 0001-Disable-wrapping-memcpy-for-Fedora.patch

BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby-devel
# Compiler is required for build of gem binary extension.
# https://fedoraproject.org/wiki/Packaging:C_and_C++#BuildRequires_and_Requires
BuildRequires: gcc
# BuildRequires: rubygem(rake-compiler-dock) = 1.2.1
# BuildRequires: rubygem(rake-compiler) >= 1.1.0
# BuildRequires: rubygem(rake-compiler) < 1.2
# BuildRequires: rubygem(test-unit)
# BuildRequires: rubygem(test-unit) >= 3.0
# BuildRequires: rubygem(test-unit) < 4
# BuildRequires: rubygem(test-unit) >= 3.0.9

%description
Protocol Buffers are Google's data interchange format.


%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}

%patch -P0 -p2

%build
# Create the gem as gem install only works on a gem file
gem build ../%{gem_name}-%{version}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

mkdir -p %{buildroot}%{gem_extdir_mri}
cp -a .%{gem_extdir_mri}/{gem.build_complete,google} %{buildroot}%{gem_extdir_mri}/

# Prevent dangling symlink in -debuginfo (rhbz#878863).
rm -rf %{buildroot}%{gem_instdir}/ext/


%check
pushd .%{gem_instdir}
# ruby -e 'Dir.glob "./test/**/*_test.rb", &method(:require)'
popd

%files
%dir %{gem_instdir}
%{gem_extdir_mri}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}


%changelog
* Thu Mar 16 2023 Jarek Prokop <jprokop@redhat.com> - 3.22.2-1
- Initial package
